# Demo-Android-Application

## TensorFLow Lite Bobble-Emoji-Prediction Android Application

This is an end-to-end example of hinglish data emoji prediction built with Tensorflow (Keras), and trained on Bobble Hinglish dataset. The demo app processes input hinglish sentences and predicts the desired and relevant emojis.

These instructions walk you through the steps to train and test a simple emoji prediction model, export them to TensorFlow Lite format and deploy on a mobile app.

#### Model
- A tflite model trained on ~48 lakhs sentences is deployed in this android application. 

### Android app
- Follow the steps below to build and run the sample Android app.

#### Requirements 
- Android Studio 3.2 or later. Install instructions can be found on [Android Studio](https://developer.android.com/studio/install) website.
- An Android device or an Android emulator with API level ~28.

#### Building
- Open Android Studio, and from the Welcome screen, select Open an existing Android Studio project.
- From the Open File or Project window that appears, navigate to and select the android application directory from wherever you cloned the TensorFlow Lite sample GitLab repo.
- You may also need to install various platforms and tools according to error messages.
- If it asks you to use Instant Run, click Proceed Without Instant Run.

#### Running
- You need to have an Android device plugged in with developer options enabled at this point.
- If you already have an Android emulator installed in Android Studio, select a virtual device with API level higher than 15.
- Click Run to run the demo app on your Android device.

